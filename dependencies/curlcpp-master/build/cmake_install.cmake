# Install script for directory: C:/Dev/curlcpp-master

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/CURLCPP")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "C:/Dev/curlcpp-master/include/cookie.h"
    "C:/Dev/curlcpp-master/include/cookie_date.h"
    "C:/Dev/curlcpp-master/include/cookie_datetime.h"
    "C:/Dev/curlcpp-master/include/cookie_time.h"
    "C:/Dev/curlcpp-master/include/curl_config.h"
    "C:/Dev/curlcpp-master/include/curl_cookie.h"
    "C:/Dev/curlcpp-master/include/curl_easy.h"
    "C:/Dev/curlcpp-master/include/curl_easy_info.h"
    "C:/Dev/curlcpp-master/include/curl_exception.h"
    "C:/Dev/curlcpp-master/include/curl_form.h"
    "C:/Dev/curlcpp-master/include/curl_global.h"
    "C:/Dev/curlcpp-master/include/curl_header.h"
    "C:/Dev/curlcpp-master/include/curl_info.h"
    "C:/Dev/curlcpp-master/include/curl_interface.h"
    "C:/Dev/curlcpp-master/include/curl_ios.h"
    "C:/Dev/curlcpp-master/include/curl_multi.h"
    "C:/Dev/curlcpp-master/include/curl_option.h"
    "C:/Dev/curlcpp-master/include/curl_pair.h"
    "C:/Dev/curlcpp-master/include/curl_receiver.h"
    "C:/Dev/curlcpp-master/include/curl_sender.h"
    "C:/Dev/curlcpp-master/include/curl_share.h"
    "C:/Dev/curlcpp-master/include/curl_utility.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("C:/Dev/curlcpp-master/build/src/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "C:/Dev/curlcpp-master/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
