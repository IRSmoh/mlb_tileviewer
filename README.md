Demo video: https://www.youtube.com/watch?v=ObZBYzDA_pI

Compilation instructions:

Use Visual Studio 2017 or greater (since we need c++17 support)

If compilation initially fails, change the windows SDK version the project is building against to _any_ available.

Otherwise hit f7 to build and f5 to run. .exe's can be run from their built folder.

Only 64bit is supported (32bit would be trivial, it just requires linking a ton of libraries)

CONTROLS:

left arrow -> move left by 1 tile

right arrow -> move right by 1 tile

up arrow -> increase tile size

down arrow -> decrease tile size


Enter (return) or Enter (keypad) to display blurb "expanded info"

Hit escape to end the program, or hit the exit button either or.