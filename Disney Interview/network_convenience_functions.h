#pragma once
#include <string>
#include <sstream>

#include <curl_easy.h>
#include <curl_ios.h>
#include <curl_exception.h>
auto read_from_url(const std::string& url)
{
	std::stringstream stream;
	try
	{
		curl::curl_ios<std::stringstream> writer(stream);
		curl::curl_easy easy(writer);

		easy.add<CURLOPT_URL>(url.data());
		easy.add<CURLOPT_FOLLOWLOCATION>(1l);

		easy.perform();
	}
	catch (const curl::curl_easy_exception& excep)
	{
		excep.print_traceback();
	}
	return stream;
}