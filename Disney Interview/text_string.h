#pragma once
#include <string>

#include <SDL.h>
#include <SDL_ttf.h>

#include <glm/glm.hpp>

#include "unique_custom_deleter.h"
struct text_string
{
	unique_custom_deleter<SDL_Surface*> surface;
	unique_custom_deleter<SDL_Texture*> texture;
	glm::ivec2 dims;

	static text_string make_text_string(const std::string& message, TTF_Font* font, SDL_Renderer* renderer, SDL_Color text_color = { 255,255,255 })
	{
		text_string text;
		text.surface = { TTF_RenderText_Blended(font, message.c_str(), text_color), [](auto surf) { if (surf) SDL_FreeSurface(surf); } };
		text.texture = { SDL_CreateTextureFromSurface(renderer, text.surface),[](auto text) { if (text) SDL_DestroyTexture(text); } };
		if (text.surface == nullptr || text.texture == nullptr)
			int n = 0;
		text.dims = {};
		int val = TTF_SizeText(font, message.c_str(), &text.dims.x, &text.dims.y);
		return text;
	}
	static std::vector<text_string> make_text_string(const std::vector<std::string>& messages, TTF_Font* font, SDL_Renderer* renderer, SDL_Color text_color = { 255,255,255 })
	{
		std::vector<text_string> texts;
		for (auto& str : messages)
		{
			texts.emplace_back(make_text_string(str, font, renderer, text_color));
		}
		return texts;
	}
};