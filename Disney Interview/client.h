#pragma once
#include <iostream>
#include <chrono>
#include <iomanip>

#include <tbb/concurrent_vector.h>

#include <SDL.h>
#include <SDL_ttf.h>
#undef main
//something in sdl defines main.. well #define main SDL_MAIN which expands to nothing.
//why they thought it was a good idea who knows, but we'll undef that so things work properly on our end.
#include <glm/glm.hpp>

#include <rapidjson/Document.h>

#include "zip_iterator.h"
#include "unique_custom_deleter.h"
#include "thread_pool.h"
#include "network_convenience_functions.h"
#include "json_reader.h"
#include "tile.h"

class client
{
	unique_custom_deleter<void*> sdl_auto_quit;
	unique_custom_deleter<void*> ttf_auto_quit;
	unique_custom_deleter<SDL_Window*> window;
	unique_custom_deleter<SDL_Renderer*> renderer;

	unique_custom_deleter<TTF_Font*> font;

	glm::ivec2 window_dims;

	bool use_todays_date;

	bool display_blurb;
public:

	void init(int font_size, bool use_todays_date_)
	{
		display_blurb = false;
		use_todays_date = use_todays_date_;
		if (SDL_Init(SDL_INIT_VIDEO) != 0)
		{
			std::cout << "sdl_init error: " << SDL_GetError() << "\n";
			return;
		}
		SDL_DisplayMode screen_size;
		SDL_GetCurrentDisplayMode(0, &screen_size);
		window_dims = { screen_size.w,screen_size.h };
		sdl_auto_quit = { nullptr,[](auto ignored) {SDL_Quit(); } };

		if (TTF_Init() == -1)
		{
			std::cout << "font init error\n";
			return;
		}
		ttf_auto_quit = { nullptr,[](auto ignored) {TTF_Quit(); } };

		window = { SDL_CreateWindow("Disney Interview", 0, 0, window_dims.x, window_dims.y, SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS),[](auto win) {SDL_DestroyWindow(win); } };
		if (window == nullptr)
		{
			std::cout << "window init error\n";
			return;
		}
		renderer = { SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC), [](auto rend) {SDL_DestroyRenderer(rend); } };
		if (renderer == nullptr)
		{
			std::cout << "renderer init error\n";
			return;
		}

		std::string font_filepath = "../dependencies/arial.ttf";
		font = { TTF_OpenFont(font_filepath.c_str(), font_size), [](auto font) {if (font) TTF_CloseFont(font); } };

		if (font == nullptr)
		{
			std::cout << "font init error\n";
			return;
		}
	}
	void run()
	{
		std::string dynamic_game_url_base = "http://statsapi.mlb.com/api/v1/schedule?hydrate=game(content(editorial(recap))),decisions&date=";
		std::string dynamic_game_url_end = "&sportId=1";
		std::string date_to_load_str;
		if (use_todays_date)
		{
			date_to_load_str = date_to_iso_str(std::chrono::system_clock::now());
		}
		else
		{
			date_to_load_str = "2018-06-10";
		}

		tile_manager tiles_manager(window_dims,std::move(dynamic_game_url_base),std::move(dynamic_game_url_end));

		tiles_manager.create_loading_text(renderer, font);
		tiles_manager.get_images_from_url(date_to_load_str);

		std::string background_filepath = "../dependencies/background.jpg";
		texture background_texture = texture::generate_texture(background_filepath, renderer);
		bool kill_program = false;

		auto start_time = std::chrono::high_resolution_clock::now();
		auto end_time = start_time;
		while (true)
		{
			start_time = end_time;
			end_time = std::chrono::high_resolution_clock::now();

			double delta_time = std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time).count();
			tiles_manager.upload_tiles(font, renderer);

			SDL_Event event;
			while (SDL_PollEvent(&event))
			{
				if (event.type == SDL_QUIT)
				{
					kill_program = true;
					break;
				}
				else if (event.type == SDL_KEYDOWN)
				{
					switch (event.key.keysym.sym)
					{
					case SDLK_ESCAPE:
						kill_program = true;
						break;
					case SDLK_LEFT:
						display_blurb = !tiles_manager.decrement_active_index();
						break;
					case SDLK_RIGHT:
						display_blurb = !tiles_manager.increment_active_index();
						break;
					case SDLK_UP:
						tiles_manager.increase_tile_size();
						break;
					case SDLK_DOWN:
						tiles_manager.decrease_tile_size();
						break;
					case SDLK_RETURN:
					case SDLK_KP_ENTER:
						display_blurb = !display_blurb;
						break;
					}
					

				}
			}
			if (kill_program)
				break;
			SDL_RenderClear(renderer);
			SDL_RenderCopy(renderer, background_texture.sdl_tex, nullptr, nullptr);
			tiles_manager.render(renderer,display_blurb);

			SDL_RenderPresent(renderer);
		}
	}
};