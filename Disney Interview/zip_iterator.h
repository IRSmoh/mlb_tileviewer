#pragma once
#include <numeric>
#include <tuple>
#include <type_traits>
#include <array>
#include <utility>
namespace zip
{
	struct default_equality_tag {};
	//terribly named..
	//but signals that == and != will check per sub iterator for comparison
	//e.g. if one iterator is shorter than another and thus ends sooner this will cause them to compare equivalent.
	struct fuzzy_equality_tag
	{
		template<typename tuple_type, std::size_t ... inds>
		static bool fuzzy_compare(const tuple_type& left, const tuple_type& right, std::index_sequence<inds...>)
		{
			return ((std::get<inds>(left) == std::get<inds>(right)) || ... || false);
		}
	};
	template<typename swap_type, std::size_t... inds>
	void tuple_swap(swap_type& left, swap_type& right, std::index_sequence<inds...>)
	{
		(std::swap(std::get<inds>(left), std::get<inds>(right)), ...);
	}
	struct pseudo_lvalue_tag {};
	struct pseudo_rvalue_tag {};
	template<typename tag_type = pseudo_lvalue_tag, typename ... tuple_types>
	struct tuple_wrapper
	{
	private:
		template<typename type, std::enable_if_t<std::is_move_assignable<type>::value, bool> = true>
		static void copy_or_move(type& left, type& right)
		{
			left = std::move(right);
		}
		template<typename type, std::enable_if_t<!std::is_move_assignable<type>::value, bool> = false>
		static void copy_or_move(type& left, type& right)
		{
			left = right;
		}
		template<typename assign_or_move_tuple, std::size_t... inds>
		static void copy_or_move(assign_or_move_tuple& left, assign_or_move_tuple& right, std::index_sequence<inds...>)
		{
			(copy_or_move(std::get<inds>(left), std::get<inds>(right)), ...);
		}
	public:
		using ref_tuple = std::tuple<tuple_types&...>;
		using self_as_lvalue = tuple_wrapper<pseudo_lvalue_tag, tuple_types...>;
		using self_as_rvalue = tuple_wrapper<pseudo_rvalue_tag, tuple_types...>;

		ref_tuple wrapped_tuple;
		tuple_wrapper(const ref_tuple& tup, char tag) : wrapped_tuple(tup) {}
		tuple_wrapper(ref_tuple&& tup) : wrapped_tuple(std::move(tup)) {}

		template<typename tag = tag_type, std::enable_if_t<std::is_same<tag, pseudo_rvalue_tag>::value, bool> = true>
		tuple_wrapper(tuple_wrapper<pseudo_lvalue_tag, tuple_types...>&& right) : wrapped_tuple(std::move(right.wrapped_tuple))
		{}
		template<typename conv_type, typename tag = tag_type, std::enable_if_t<std::is_same<tag, pseudo_lvalue_tag>::value && !std::is_same<conv_type, ref_tuple>::value && !std::is_same<conv_type, tuple_wrapper>::value, bool> = true>
		operator conv_type() &&
		{
			return conversion_helper_copy<conv_type>(std::make_index_sequence<sizeof...(tuple_types)>{});
		}
		template<typename conv_type, typename tag = tag_type, std::enable_if_t<std::is_same<tag, pseudo_rvalue_tag>::value && !std::is_same<conv_type, ref_tuple>::value && !std::is_same<conv_type, tuple_wrapper>::value, bool> = true>
		operator conv_type() &&
		{
			return conversion_helper_move<conv_type>(std::make_index_sequence<sizeof...(tuple_types)>{});
		}
		template<typename conv_type>
		operator conv_type() const&
		{
			return conversion_helper_copy<conv_type>(std::make_index_sequence<sizeof...(tuple_types)>{});
		}
		template<typename conv_type>
		operator conv_type() &
		{
			return conversion_helper_copy<conv_type>(std::make_index_sequence<sizeof...(tuple_types)>{});
		}


		tuple_wrapper(const tuple_wrapper& right) : wrapped_tuple(right.wrapped_tuple)
		{}
		tuple_wrapper(tuple_wrapper&& right) : wrapped_tuple(std::move(right.wrapped_tuple)) {}
		tuple_wrapper& operator=(const tuple_wrapper& right)
		{
			wrapped_tuple = right.wrapped_tuple;
			return *this;
		}
		tuple_wrapper& operator=(tuple_wrapper&& right)
		{
			copy_or_move(wrapped_tuple, right.wrapped_tuple, std::make_index_sequence<std::tuple_size<ref_tuple>::value>{});
			return *this;
		}
		template<typename tuple_left, typename tuple_right, std::size_t ... inds, std::enable_if_t<!std::is_same<tuple_right, self_as_lvalue>::value && !std::is_same<tuple_right, self_as_rvalue>::value, bool> = true>
		static void copy_assign(tuple_left& left, tuple_right& right, std::index_sequence<inds...>)
		{
			(void(std::get<inds>(left) = std::get<inds>(right)), ...);
		}
		template<typename tuple_left, typename tuple_right, std::size_t ... inds, std::enable_if_t<std::is_same<tuple_right, self_as_lvalue>::value || std::is_same<tuple_right, self_as_rvalue>::value, bool> = true>
		static void copy_assign(tuple_left& left, tuple_right& right, std::index_sequence<inds...>)
		{
			(void(std::get<inds>(left) = std::get<inds>(right.wrapped_tuple)), ...);
		}
		template<typename tuple_type>
		tuple_wrapper& operator=(const tuple_type& right)
		{
			copy_assign(wrapped_tuple, right, std::make_index_sequence<std::tuple_size<ref_tuple>::value>{});
			return *this;
		}

		template<typename tuple_left, typename tuple_right, std::size_t ... inds, std::enable_if_t<!std::is_same<tuple_right, self_as_lvalue>::value && !std::is_same<tuple_right, self_as_rvalue>::value, bool> = true>
		static void move_assign(tuple_left& left, tuple_right& right, std::index_sequence<inds...>)
		{
			(void(std::get<inds>(left) = std::move(std::get<inds>(right))), ...);
		}
		template<typename tuple_left, typename tuple_right, std::size_t ... inds, std::enable_if_t<std::is_same<tuple_right, self_as_lvalue>::value || std::is_same<tuple_right, self_as_rvalue>::value, bool> = true>
		static void move_assign(tuple_left& left, tuple_right& right, std::index_sequence<inds...>)
		{
			(void(std::get<inds>(left) = std::move(std::get<inds>(right.wrapped_tuple))), ...);
		}
		template<typename tuple_type, std::enable_if_t<!std::is_same<tuple_type, tuple_wrapper<pseudo_lvalue_tag, tuple_types...>>::value || !std::is_same<tuple_type, tuple_wrapper<pseudo_rvalue_tag, tuple_types...> >::value, bool> = true>
		tuple_wrapper& operator=(tuple_type&& right)
		{
			move_assign(wrapped_tuple, right, std::make_index_sequence<std::tuple_size<ref_tuple>::value>{});
			return *this;
		}

		static void swap(tuple_wrapper& left, tuple_wrapper& right)
		{
			tuple_swap(left.wrapped_tuple, right.wrapped_tuple, std::make_index_sequence<std::tuple_size<ref_tuple>::value>{});
		}
		bool operator<(const tuple_wrapper& right) const noexcept
		{
			return wrapped_tuple < right.wrapped_tuple;
		}
		bool operator==(const tuple_wrapper& right) const noexcept
		{
			return wrapped_tuple == right.wrapped_tuple;
		}
		bool operator!=(const tuple_wrapper& right) const noexcept
		{
			return !(*this == right);
		}
		bool operator>(const tuple_wrapper& right) const noexcept
		{
			return wrapped_tuple > right.wrapped_tuple;
		}
		bool operator<=(const tuple_wrapper& right) const noexcept
		{
			return !(*this > right);
		}
		bool operator>=(const tuple_wrapper& right) const noexcept
		{
			return !(*this < right);
		}
		template<typename obj_tuple>
		bool operator<(const obj_tuple& right) const noexcept
		{
			return wrapped_tuple < right;
		}
		template<typename obj_tuple>
		bool operator==(const obj_tuple& right) const noexcept
		{
			return wrapped_tuple == right;
		}
		template<typename obj_tuple>
		bool operator!=(const obj_tuple& right) const noexcept
		{
			return !(*this == right);
		}
		template<typename obj_tuple>
		bool operator>(const obj_tuple& right) const noexcept
		{
			return wrapped_tuple > right;
		}
		template<typename obj_tuple>
		bool operator<=(const obj_tuple& right) const noexcept
		{
			return !(*this > right);
		}
		template<typename obj_tuple>
		bool operator>=(const obj_tuple& right) const noexcept
		{
			return !(*this < right);
		}
		template<typename obj_tuple>
		friend bool operator<(const obj_tuple& left, const tuple_wrapper& right) noexcept
		{
			return left < right.wrapped_tuple;
		}
		template<typename obj_tuple>
		friend bool operator==(const obj_tuple& left, const tuple_wrapper& right) noexcept
		{
			return left == right.wrapped_tuple;
		}
		template<typename obj_tuple>
		friend bool operator!=(const obj_tuple& left, const tuple_wrapper& right) noexcept
		{
			return left != right.wrapped_tuple;
		}
		template<typename obj_tuple>
		friend bool operator>(const obj_tuple& left, const tuple_wrapper& right) noexcept
		{
			return left > right.wrapped_tuple;
		}
		template<typename obj_tuple>
		friend bool operator>=(const obj_tuple& left, const tuple_wrapper& right) noexcept
		{
			return left >= right.wrapped_tuple;
		}
		template<typename obj_tuple>
		friend bool operator<=(const obj_tuple& left, const tuple_wrapper& right) noexcept
		{
			return left <= right.wrapped_tuple;
		}

		template<std::size_t ind, typename type, std::enable_if_t<std::is_same_v<std::decay_t<type>, tuple_wrapper>, bool> = true>
		friend std::tuple_element_t<ind, std::tuple<tuple_types&...>> get(type&& itr)
		{
			return std::get<ind>(itr.wrapped_tuple);
		}

	private:
		template<typename type, std::enable_if_t<std::is_move_constructible<type>::value, bool> = true>
		static type constructor_or_move(type& data)
		{
			return std::move(data);
		}
		template<typename type, std::enable_if_t<!std::is_move_constructible<type>::value, bool> = false>
		static type constructor_or_move(type& data)
		{
			return data;
		}
		template<typename conv_type, std::size_t ... inds>
		conv_type conversion_helper_copy(std::index_sequence<inds...>) const
		{
			return conv_type{ std::get<inds>(wrapped_tuple)... };
		}
		template<typename conv_type, std::size_t ... inds>
		conv_type conversion_helper_move(std::index_sequence<inds...>) const
		{
			return conv_type{ constructor_or_move(std::get<inds>(wrapped_tuple))... };
		}
	};
	template<typename equality_tag, typename itr_type, typename... rest>
	class iterator
	{
	protected:
		using itr_tuple = std::tuple<itr_type, rest...>;
		itr_tuple itrs;

	public:

		//a "magic" class that when supplied with indicies will allow us to unpack a tuple that contains iterators, derefence them, and rebuild a tuple from that.
		//this is so we can mimic a normal iterator, but apply it to multiple (e.g. a proper zip_iterator.)
		template<typename ... garbage>struct get_tuple_as {};
		template<std::size_t... indices>
		struct get_tuple_as<std::index_sequence<indices...>>
		{
			template<typename ... types>
			using tuple_with_remove_references = std::tuple<typename std::remove_reference<types>::type...>;

			using tuple_with_references = std::tuple<decltype(*std::get<indices>(itr_tuple()))...>;


			using ref_tuple = tuple_wrapper<pseudo_lvalue_tag, decltype(*std::get<indices>(itr_tuple()))...>;
			using obj_tuple = tuple_with_remove_references<decltype(*std::get<indices>(itr_tuple()))...>;
			using ptr_tuple = std::tuple<decltype(&(*std::get<indices>(itr_tuple())))...>;

			static ref_tuple refs(const itr_tuple& tuple_itrs)
			{
				return ref_tuple(std::move(tuple_with_references(*std::get<indices>(tuple_itrs)...)), false);
			}
			static obj_tuple objs(const itr_tuple& tuple_itrs)
			{
				return obj_tuple(*std::get<indices>(tuple_itrs)...);
			}
			static void increment(itr_tuple& tuple_itrs)
			{
				char op[] = { (++std::get<indices>(tuple_itrs), '\0')... };	//hack for allowing us to expand our tuple and call our operator on it.
				(void)op;	//supress unused variable for those compilers that would complain.
			}
			static void decrement(itr_tuple& tuple_itrs)
			{
				char op[] = { (--std::get<indices>(tuple_itrs), '\0')... };
				(void)op;
			}
			static itr_tuple increment_distance(itr_tuple& tuple_itrs, ptrdiff_t distance)
			{
				return itr_tuple((std::get<indices>(tuple_itrs) + distance)...);
			}
		};
		template<std::size_t ... indx>
		decltype(auto) refs(std::index_sequence<indx...>)
		{
			return std::tuple<decltype(*std::get<indx>(itrs))...>(*std::get<indx>(itrs)...);
		}
		using itr_tuple_unpacker = get_tuple_as<std::make_index_sequence<std::tuple_size<itr_tuple>::value>>;

		using reference = typename itr_tuple_unpacker::ref_tuple;
		using value_type = typename itr_tuple_unpacker::obj_tuple;
		using pointer = typename itr_tuple_unpacker::ptr_tuple;
		using difference_type = ptrdiff_t;

		iterator() {}
		iterator(const std::tuple<itr_type, rest...>& itr_pack) : itrs(itr_pack) {}
		iterator(const itr_type& first_itr, const rest&... rest_itrs) :
			itrs(itr_tuple(first_itr, rest_itrs...)) {}
		iterator(itr_type&& first_itr, rest&&... rest_itrs) :
			itrs(itr_tuple(std::forward<itr_type>(first_itr), std::forward<rest>(rest_itrs)...)) {}

		void increment()
		{
			itr_tuple_unpacker::increment(itrs);
		}
		friend void swap(iterator& left, iterator& right)
		{
			std::swap(left, right);
		}
		iterator& operator++()
		{
			increment();
			return *this;
		}
		iterator operator++(int)
		{
			auto tmp = *this;
			increment();
			return tmp;
		}
		friend void iter_swap(iterator& left, iterator& right)
		{
			tuple_swap((*left).wrapped_tuple, (*right).wrapped_tuple, std::make_index_sequence<std::tuple_size<typename reference::ref_tuple>::value>{});
		}

	};
	//normally a forward iterator would inherit from output and input iterator.. but to do so we have to use virtual inheritance on the base iterator type.
	//this causes our iterator to be 1 virtual pointer larger than it techinically has to.
	//also, no stl algorithm/struct uses just output/input... so we can make them redudant and just roll it into forward_iterator.

	template<typename equality_tag, typename itr_type, typename... rest>
	class forward_iterator : public iterator<equality_tag, itr_type, rest...>
	{
		using base_type = iterator<equality_tag, itr_type, rest...>;
	public:
		using iterator_category = std::forward_iterator_tag;
		using iterator<equality_tag, itr_type, rest...>::iterator;

		typename base_type::reference operator*() const
		{
			return base_type::itr_tuple_unpacker::refs(base_type::itrs);
		}
		template<typename equal_tag = equality_tag, std::enable_if_t<std::is_same<equal_tag, default_equality_tag>::value, bool> = true>
		bool operator==(const forward_iterator& right) const
		{
			return base_type::itrs == right.itrs;
		}
		template<typename equal_tag = equality_tag, std::enable_if_t<std::is_same<equal_tag, default_equality_tag>::value, bool> = true>
		bool operator!=(const forward_iterator& right) const
		{
			return !(*this == right);
		}
		template<typename equal_tag = equality_tag, std::enable_if_t<std::is_same<equal_tag, fuzzy_equality_tag>::value, bool> = true>
		bool operator==(const forward_iterator& right) const
		{
			return fuzzy_equality_tag::fuzzy_compare(base_type::itrs, right.itrs, std::make_index_sequence<std::tuple_size<typename base_type::itr_tuple>::value>{});
		}
		template<typename equal_tag = equality_tag, std::enable_if_t<std::is_same<equal_tag, fuzzy_equality_tag>::value, bool> = true>
		bool operator!=(const forward_iterator& right) const
		{
			return !(*this == right);
		}
		forward_iterator& operator++()
		{
			base_type::increment();
			return *this;
		}
		forward_iterator operator++(int)
		{
			auto tmp = *this;
			base_type::increment();
			return tmp;
		}
	};
	template<typename equality_tag, typename itr_type, typename... rest>
	class bidirectional_iterator : public forward_iterator<equality_tag, itr_type, rest...>
	{
		using base_type = iterator<equality_tag, itr_type, rest...>;
	public:
		using iterator_category = std::bidirectional_iterator_tag;
		using forward_iterator<equality_tag, itr_type, rest...>::forward_iterator;

		void decrement()
		{
			base_type::itr_tuple_unpacker::decrement(base_type::itrs);
		}
		bidirectional_iterator& operator--()
		{
			decrement();
			return *this;
		}
		bidirectional_iterator operator--(int)
		{
			auto tmp = *this;
			decrement();
			return tmp;
		}
		bidirectional_iterator& operator++()
		{
			base_type::increment();
			return *this;
		}
		bidirectional_iterator operator++(int)
		{
			auto tmp = *this;
			base_type::increment();
			return tmp;
		}
	};

	template<typename equality_tag, typename itr_type, typename... rest>
	class random_access_iterator : public bidirectional_iterator<equality_tag, itr_type, rest...>
	{
		using base_type = iterator<equality_tag, itr_type, rest...>;
	public:
		using iterator_category = std::random_access_iterator_tag;
		using bidirectional_iterator<equality_tag, itr_type, rest...>::bidirectional_iterator;


		bool operator<(const random_access_iterator& right) const
		{
			return base_type::itrs < right.itrs;
		}
		bool operator>(const random_access_iterator& right) const
		{
			return base_type::itrs > right.itrs;
		}
		bool operator<=(const random_access_iterator& right) const
		{
			return !(*this > right);
		}
		bool operator>=(const random_access_iterator& right) const
		{
			return !(*this < right);
		}
		random_access_iterator& operator++()
		{
			base_type::increment();
			return *this;
		}
		random_access_iterator operator++(int)
		{
			auto tmp = *this;
			base_type::increment();
			return tmp;
		}
		random_access_iterator operator+(ptrdiff_t diff)
		{
			return base_type::itr_tuple_unpacker::increment_distance(base_type::itrs, diff);
		}
		random_access_iterator& operator+=(ptrdiff_t diff)
		{
			return *this = *this + diff;
		}
		friend random_access_iterator operator+(ptrdiff_t diff, const random_access_iterator& right)
		{
			return right + diff;
		}
		random_access_iterator operator-(ptrdiff_t diff)
		{
			return *this + (-diff);
		}
		random_access_iterator& operator-=(ptrdiff_t diff)
		{
			return *this = *this - diff;
		}
		typename base_type::difference_type operator-(const random_access_iterator& right) const
		{
			return std::get<0>(base_type::itrs) - std::get<0>(right.itrs);
		}
		typename base_type::reference operator [](size_t offset)
		{
			return base_type::itr_tuple_unpacker::refs(base_type::itr_tuple_unpacker::increment_distance(base_type::itrs, offset));
		}
	};

	using equality_tag_status = default_equality_tag;

	template<typename equality_tag = equality_tag_status, typename ... types>
	random_access_iterator<equality_tag, types...> random(types&&... itrs)
	{
		return random_access_iterator<equality_tag, types...>(std::forward<types>(itrs)...);
	}
	template<typename equality_tag = equality_tag_status, typename ... types>
	bidirectional_iterator<equality_tag, types...> bidirectional(types&&... itrs)
	{
		return bidirectional_iterator<equality_tag, types...>(std::forward<types>(itrs)...);
	}
	template<typename equality_tag = equality_tag_status, typename...types>
	forward_iterator<equality_tag, types...> forward(types&&... itrs)
	{
		return forward_iterator<equality_tag, types...>(std::forward<types>(itrs)...);
	}
	template<typename itr_type>
	struct itrs
	{
		itr_type begin_, end_;
		itr_type begin() const
		{
			return begin_;
		}
		itr_type end() const
		{
			return end_;
		}
	};
	template<typename itr_type>
	auto make_itrs(itr_type&& begin, itr_type&& end)
	{
		return itrs<itr_type>{begin, end};
	}
	namespace impl
	{
		template<typename itr_access_tag>
		struct minimum_itr_access_tag_value
		{
			constexpr static std::size_t value = 0;
		};
		template<>
		struct minimum_itr_access_tag_value<std::bidirectional_iterator_tag>
		{
			constexpr static std::size_t value = 1;
		};
		template<>
		struct minimum_itr_access_tag_value<std::random_access_iterator_tag>
		{
			constexpr static std::size_t value = 2;
		};
		template<typename... types>
		constexpr auto get_min_itr_cat()
		{
			constexpr std::array itr_cat_values = { impl::minimum_itr_access_tag_value<typename std::iterator_traits<decltype(begin(std::declval<std::decay_t<types>>()))>::iterator_category>::value... };
			return [&itr_cat_values]()
			{
				auto lowest_val = *itr_cat_values.begin();
				for (auto& val : itr_cat_values)
				{
					if (val < lowest_val)
						lowest_val = val;
				}
				return lowest_val;
			}();
		}
		template<typename comparison_tag, typename... types>
		auto fit_range_impl(types&&... containers)
		{
			constexpr auto min_itr_cat = get_min_itr_cat<types...>();
			if constexpr (min_itr_cat == 0)
				return make_itrs(forward<comparison_tag>(std::begin(containers)...), forward<comparison_tag>(std::end(containers)...));

			if constexpr (min_itr_cat == 1)
				return make_itrs(bidirectional<comparison_tag>(std::begin(containers)...), bidirectional<comparison_tag>(std::end(containers)...));

			else
				return make_itrs(random<comparison_tag>(std::begin(containers)...), random<comparison_tag>(std::end(containers)...));
		}
		template<typename comparison_tag, typename... types>
		auto fit_range_reverse_impl(types&&... containers)
		{
			constexpr auto min_itr_cat = get_min_itr_cat<types...>();
			if constexpr (min_itr_cat == 0)
				return make_itrs(forward<comparison_tag>(std::rbegin(containers)...), forward<comparison_tag>(std::rend(containers)...));

			if constexpr (min_itr_cat == 1)
				return make_itrs(bidirectional<comparison_tag>(std::rbegin(containers)...), bidirectional<comparison_tag>(std::rend(containers)...));

			else
				return make_itrs(random<comparison_tag>(std::rbegin(containers)...), random<comparison_tag>(std::rend(containers)...));
		}
	}
	template<typename ... types>
	auto range(types&&... containers)
	{
		return impl::fit_range_impl<fuzzy_equality_tag>(std::forward<types>(containers)...);
	}
	template<typename ... types>
	auto range_unchecked(types&&... containers)
	{
		return impl::fit_range_impl<default_equality_tag>(std::forward<types>(containers)...);
	}
	template<typename... types>
	auto range_reverse_unchecked(types&&... containers)
	{
		return impl::fit_range_reverse_impl<default_equality_tag>(std::forward<types>(containers)...);
	}
	template<typename... types>
	auto range_reverse(types&&... containers)
	{
		return impl::fit_range_reverse_impl<fuzzy_equality_tag>(std::forward<types>(containers)...);
	}

}

namespace std
{
	template<typename ... types>
	constexpr zip::tuple_wrapper<zip::pseudo_rvalue_tag, types...> move(zip::tuple_wrapper<zip::pseudo_lvalue_tag, types...>& right) noexcept
	{
		return { std::move(right.wrapped_tuple) };
	}

	template<typename ... types>
	void swap(zip::tuple_wrapper<zip::pseudo_lvalue_tag, types...>&& left, zip::tuple_wrapper<zip::pseudo_lvalue_tag, types...>&& right)
	{
		using ref_tuple = typename zip::tuple_wrapper<zip::pseudo_lvalue_tag, types...>::ref_tuple;
		zip::tuple_swap(left.wrapped_tuple, right.wrapped_tuple, std::make_index_sequence<std::tuple_size<ref_tuple>::value>{});
	}
	template<typename ... types>
	void swap(std::tuple<types&...>&& left, std::tuple<types&...>&& right)
	{
		left.swap(right);
	}
	template<typename tag, typename ... types>
	struct tuple_size<zip::tuple_wrapper<tag, types...>> :
		std::integral_constant<std::size_t, sizeof...(types)> {};

	template<std::size_t ind, typename tag, typename...types>
	struct tuple_element<ind, zip::tuple_wrapper<tag, types...>> :
		std::tuple_element<ind, std::tuple<types&...>>
	{};
}