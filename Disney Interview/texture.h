#pragma once
#include <filesystem>

#include <glm/glm.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <SDL.h>

#include "unique_custom_deleter.h"
struct texture
{
	glm::ivec2 image_dims;
	int channel_count;
	unique_custom_deleter<unsigned char*> texture_texels;
	unique_custom_deleter<SDL_Surface*> surface;
	unique_custom_deleter<SDL_Texture*> sdl_tex;

	//this is a networked call, and may take a sizeable amount of time to actually complete.
	static texture generate_texture(unsigned char* texels, int image_size_in_bytes, SDL_Renderer* renderer)
	{
		texture text{};

		text.texture_texels =
		{
			stbi_load_from_memory(texels,
									image_size_in_bytes,
									&text.image_dims.x,
									&text.image_dims.y,
									&text.channel_count,
									STBI_rgb),
			[](auto ptr) {stbi_image_free(ptr); }
		};

		text.surface = { SDL_CreateRGBSurfaceWithFormatFrom(
															text.texture_texels,
															text.image_dims.x,
															text.image_dims.y,
															1,
															text.image_dims.x*text.channel_count,
															SDL_PIXELFORMAT_RGB24),
					[](auto surf) {SDL_FreeSurface(surf); } };

		text.sdl_tex = { SDL_CreateTextureFromSurface(renderer, text.surface),[](auto tex) {SDL_DestroyTexture(tex); } };

		return text;
	}
	static texture generate_texture(const std::filesystem::path& image_path, SDL_Renderer* renderer)
	{
		texture text;
		text.image_dims;
		text.channel_count;
		text.texture_texels =
		{
			stbi_load(
				image_path.generic_string().c_str(),
				&text.image_dims.x,
				&text.image_dims.y,
				&text.channel_count,
				STBI_rgb),
			[](auto ptr) {stbi_image_free(ptr); }
		};

		text.surface = { SDL_CreateRGBSurfaceWithFormatFrom(
															text.texture_texels,
															text.image_dims.x,
															text.image_dims.y,
															1,
															text.image_dims.x*text.channel_count,
															SDL_PIXELFORMAT_RGB24),
					[](auto surf) {SDL_FreeSurface(surf); } };

		text.sdl_tex = { SDL_CreateTextureFromSurface(renderer, text.surface),[](auto tex) {SDL_DestroyTexture(tex); } };

		return text;
	}
};