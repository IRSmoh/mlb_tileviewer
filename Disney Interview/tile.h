#pragma once
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <chrono>

#include <tbb/concurrent_queue.h>

#include <SDL.h>

#include <glm/glm.hpp>

#include "texture.h"
#include "text_string.h"
#include "zip_iterator.h"
#include "string_manip_functions.h"
#include "date_manip_functions.h"
using time_point = decltype(std::chrono::system_clock::now());
struct tile_creation_info
{
	std::stringstream tile_texels;
	std::string headline;
	std::string home;
	std::string away;
	std::string blurb;

	std::size_t original_index;

	time_point query_date;
	bool push_front;
};
struct text_block
{
	enum class header_status
	{
		header,
		footer
	};
	enum class justification
	{
		left,
		center
	};
	static constexpr int text_padding_image = 10;
	static constexpr int text_padding_text = 5;
	std::vector<text_string> texts;

	int text_block_height;
	int text_block_max_width;
	void add_texts(std::vector<text_string>&& texts_to_add)
	{
		for (auto&& text : texts_to_add)
		{
			texts.emplace_back(std::move(text));
		}
	}
	void calculate_text_block_extremes()
	{
		text_block_height = text_padding_image - text_padding_text;
		for (auto& text : texts)
		{
			text_block_height += text.dims.y + text_padding_text;
			if (text.dims.x > text_block_max_width)
				text_block_max_width = text.dims.x;
		}
	}
	void render(SDL_Renderer* renderer, glm::ivec2 render_root_pos, header_status head_status, justification justification_status)
	{
		int curr_text_ind = -1;
		int working_text_height = head_status == header_status::header ? text_block_height : 0;
		for (auto& text : texts)
		{
			++curr_text_ind;

			SDL_Rect text_rect;
			text_rect.w = text.dims.x;
			text_rect.h = text.dims.y;

			if (justification_status == justification::center)
				text_rect.x = render_root_pos.x - (text.dims.x / 2);

			else if (justification_status == justification::left)
				text_rect.x = render_root_pos.x - (text_block_max_width / 2);

			if (head_status == header_status::header)
			{
				text_rect.y = render_root_pos.y - working_text_height;
				working_text_height -= text.dims.y + text_padding_text;
			}
			else if (head_status == header_status::footer)
			{
				text_rect.y = render_root_pos.y + text_padding_image + working_text_height;
				working_text_height += text.dims.y + text_padding_text;
			}

			SDL_RenderCopy(renderer, text.texture, nullptr, &text_rect);
		}
	}
};
struct tile
{
	glm::ivec2 tile_size;

	texture tile_texture;

	text_block header_text;
	text_block footer_text;
	text_block blurb;
	tile(texture&& tile_text_, text_block&& header_, text_block&& footer_, text_block&& blurb_) :
		tile_texture(std::move(tile_text_)),
		header_text(std::move(header_)),
		footer_text(std::move(footer_)),
		blurb(std::move(blurb_))
	{}
	void render(SDL_Renderer* renderer, glm::ivec2 root_pos, float tile_scale, bool display_text, bool display_blurb)
	{
		SDL_Rect tile_dst;
		glm::ivec2 tile_scaled = glm::vec2(tile_size)*tile_scale;
		auto tile_top_left = root_pos - tile_scaled / 2;

		tile_dst.w = tile_scaled.x;
		tile_dst.h = tile_scaled.y;
		tile_dst.x = tile_top_left.x;
		tile_dst.y = tile_top_left.y;

		SDL_RenderCopy(renderer, tile_texture.sdl_tex, nullptr, &tile_dst);
		
		if (!display_text)
			return;

		auto text_center = root_pos - glm::ivec2{ 0,tile_scaled.y } / 2;
		header_text.render(renderer, text_center, text_block::header_status::header, text_block::justification::center);
		
		footer_text.render(renderer, { text_center.x, text_center.y + tile_scaled.y }, text_block::header_status::footer, text_block::justification::center);

		if(display_blurb)
			blurb.render(renderer, { text_center.x, text_center.y + tile_scaled.y + footer_text.text_block_height }, text_block::header_status::footer, text_block::justification::left);
	}
};
class tile_manager
{
	glm::ivec2 window_size;
	std::string url_base;
	std::string url_end;

	std::map<std::pair<time_point, std::size_t>, tile> tiles;
	mutexed_struct<std::map<time_point, bool>, std::mutex> dates_being_loaded;

	tbb::concurrent_queue<tile_creation_info> tiles_pending_upload;
	int active_index;
	using map_itr = decltype(tiles.begin());
	int max_num_tiles_on_screen;

	std::vector<text_block> loading_text;
	std::size_t active_loading_ind;

	std::chrono::steady_clock::time_point start, end;

	double curr_time;
	double floating_tile_pos;

	//probably should always have threads as the first thing destroyed... so they're now moved to the bottom to prevent more program exiting and then it crashes - crashes
	thread_pool job_pool;

	static constexpr double transition_time = 0.5;
	static constexpr double floating_tile_update_delta = 1;
	static constexpr double floating_tile_min_epsilon = 0.005;
	static constexpr double tile_restitution_mult = 4.0;

	static constexpr int num_tiles_to_cull_at = 50;

	static constexpr int max_tile_size = 3; // really the amount its downscaled by .. minimal.. but that makes it larger by being smaller.
public:
	tile_manager(glm::ivec2 win_dims, std::string&& url_base_, std::string&& url_end_) :
		window_size(win_dims), 
		url_base(std::move(url_base_)),
		url_end(std::move(url_end_)),
		tiles(),
		active_index(0),
		max_num_tiles_on_screen(6),
		loading_text(),
		active_loading_ind(0),
		start(std::chrono::high_resolution_clock::now()),
		end(std::chrono::high_resolution_clock::now()),
		curr_time(0.0),
		floating_tile_pos(0.0),
		job_pool(1)
	{}
	void create_loading_text(SDL_Renderer* renderer, TTF_Font* font)
	{
		std::string loading_text_str = "loading";
		for (int i = 0; i < 4; ++i)
		{
			text_block loading_texture;
			loading_text_str.append(".");

			std::vector<text_string> text_strings;
			text_strings.emplace_back(text_string::make_text_string(loading_text_str, font, renderer));

			loading_texture.add_texts(std::move(text_strings));
			loading_texture.calculate_text_block_extremes();
			loading_text.emplace_back(std::move(loading_texture));
		}
	}
	void get_images_from_url(const std::string& date)
	{
		queue_game_image_fetches(str_to_chrono_time(date), true);
	}

	void upload_tiles(TTF_Font* font, SDL_Renderer* renderer)
	{
		constexpr static char new_line = '\n';
		constexpr static char space = ' ';
		constexpr static std::size_t max_chars_per_line = 80;

		tile_creation_info tile_create_info;

		while (tiles_pending_upload.try_pop(tile_create_info))
		{
			auto tile_time_point = tile_create_info.query_date;
			if (!tiles.empty() && tile_create_info.push_front)
			{
				++active_index;
			}
			auto tile_texture = texture::generate_texture(
					(unsigned char*)tile_create_info.tile_texels.str().data(),
					(int)tile_create_info.tile_texels.str().size(),
					renderer);

			text_block header_texts;
			std::string header_text = tile_create_info.home + "\nVS\n" +tile_create_info.away;

			header_texts.add_texts(
				text_string::make_text_string(
						split_string(header_text, new_line, max_chars_per_line, space),
						font,
						renderer));

			text_block footer_texts;
			footer_texts.add_texts(
				text_string::make_text_string(
					split_string(tile_create_info.headline, new_line, max_chars_per_line, space),
					font,
					renderer));

			text_block blurb;
			blurb.add_texts(
				text_string::make_text_string(
					split_string(tile_create_info.blurb, new_line, max_chars_per_line, space),
					font,
					renderer));

			header_texts.calculate_text_block_extremes();
			footer_texts.calculate_text_block_extremes();
			blurb.calculate_text_block_extremes();

			tiles.try_emplace(
				std::pair<time_point, std::size_t>{tile_time_point, tile_create_info.original_index},
				tile(std::move(tile_texture), std::move(header_texts), std::move(footer_texts), std::move(blurb)));
		}
	}
	bool increment_active_index()
	{
		if (!tiles.empty() && active_index < tiles.size() - 1)
		{
			++active_index;
			if (active_index ==  (tiles.size()-1))
				request_more_tiles(false);

			floating_tile_pos += floating_tile_update_delta;

			return true;
		}
		return false;
	}
	bool decrement_active_index()
	{
		if (!tiles.empty() && active_index > 0)
		{
			--active_index;
			if (active_index == 0)
				request_more_tiles(true);

			floating_tile_pos -= floating_tile_update_delta;

			return true;
		}
		else if (!tiles.empty())
		{
			request_more_tiles(true);
		}
		return false;
	}
	void increase_tile_size()
	{
		if (max_num_tiles_on_screen <= max_tile_size)
			return;
		--max_num_tiles_on_screen;
	}
	void decrease_tile_size()
	{
		++max_num_tiles_on_screen;
	}
	void request_more_tiles(bool prior_days)
	{
		if(prior_days)
		{
			queue_game_image_fetches(prior_day(tiles.begin()->first.first), false);
			if (tiles.size() > num_tiles_to_cull_at)
				destroy_by_day(true);
		}
		else
		{
			queue_game_image_fetches(next_day(tiles.rbegin()->first.first), true);
			if (tiles.size() > num_tiles_to_cull_at)
				destroy_by_day(false);
		}
	}
	//negative implies front, positive implies back
	void destroy_by_day(bool destroy_back)
	{
		time_point time_to_destroy = destroy_back ? tiles.rbegin()->first.first : tiles.begin()->first.first;
		if (destroy_back)
		{
			while (true)
			{
				auto itr = --tiles.erase(--tiles.end());
				if (tiles.empty() || itr->first.first != time_to_destroy)
					break;
			}
		}
		else
		{
			while (true)
			{
				auto itr = tiles.erase(tiles.begin());
				--active_index;
				if (tiles.empty() || itr->first.first != time_to_destroy)
					break;
			}
		}
	}
	void render(SDL_Renderer* renderer, bool display_blurb)
	{
		//to lerp the tiles in place.
		//on move call e.g. inc/dec
		//increase our floating target by x
		//every render call decrease the abs of the value to zero
		start = end;
		end = std::chrono::high_resolution_clock::now();
		double delta_time = std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();
		if (tiles.empty() || !dates_being_loaded.get()->empty())
		{
			curr_time += delta_time;
			if (curr_time >= transition_time)
			{
				curr_time -= transition_time;
				active_loading_ind = (active_loading_ind + 1) % loading_text.size();
			}
			loading_text[active_loading_ind].render(renderer, { window_size.x / 2,window_size.y / 4 }, text_block::header_status::header, text_block::justification::center);
			if(tiles.empty())
				return;
		}

		auto tile_motion_lam = [this](double tile_abs_dist, double dt)
		{
			if (tile_abs_dist > floating_tile_update_delta)
			{
				return tile_abs_dist*tile_abs_dist*dt*tile_restitution_mult;
			}
			else
			{
				return tile_abs_dist*dt*tile_restitution_mult;;
			}
		};
		if (glm::abs(floating_tile_pos) < floating_tile_min_epsilon)
		{
			floating_tile_pos = 0.0;
		}
		else if (floating_tile_pos < 0.0)
		{
			floating_tile_pos += tile_motion_lam(abs(floating_tile_pos), delta_time);
			if (floating_tile_pos > 0.0)
				floating_tile_pos = 0;
		}
		else if (floating_tile_pos > 0.0)
		{
			floating_tile_pos -= tile_motion_lam(abs(floating_tile_pos), delta_time);
			if (floating_tile_pos < 0.0)
				floating_tile_pos = 0;
		}
		constexpr float tile_active_scale_factor = 1.5f;
		auto tile_downscale_factor = max_num_tiles_on_screen * 1.5f;

		glm::ivec2 tile_size = glm::vec2(window_size) / tile_downscale_factor;
		glm::ivec2 tile_increment = { tile_size.x*tile_active_scale_factor, 0 };

		glm::ivec2 tile_center_pos = window_size / 2;
		tile_center_pos.x += (int)(tile_increment.x*floating_tile_pos);

		int this_index = -1;
		for (auto& curr_tile : tiles)
		{
			++this_index;
			curr_tile.second.tile_size = tile_size;
			bool is_active = this_index == active_index;
			curr_tile.second.render(renderer, tile_center_pos + (this_index - active_index)*tile_increment, is_active ? tile_active_scale_factor : 1.f, is_active, display_blurb);
		}
	}


	struct required_json_info
	{
		std::vector<std::string> images;
		std::vector<std::string> headlines;
		std::vector<std::string> home_teams;
		std::vector<std::string> away_teams;

		std::vector<std::string> blurbs;

		auto get_zip_range()
		{
			return zip::range(images, headlines, home_teams, away_teams, blurbs);
		}
		auto get_zip_range_reverse()
		{
			return zip::range_reverse(images, headlines, home_teams, away_teams, blurbs);
		}
	};
	template<typename json_type>
	required_json_info get_game_info(const json_type& json)
	{
		try
		{
			required_json_info info;
			auto image_path = "dates/games/content/editorial/recap/home/photo/cuts/684x385/src";
			auto headline_path = "dates/games/content/editorial/recap/home/headline";
			auto home_team_path = "dates/games/teams/home/team/name";
			auto away_team_path = "dates/games/teams/away/team/name";

			auto blurb_path = "dates/games/content/editorial/recap/mlb/blurb";


			json_reader(json, image_path, info.images);
			json_reader(json, headline_path, info.headlines);
			json_reader(json, home_team_path, info.home_teams);
			json_reader(json, away_team_path, info.away_teams);

			json_reader(json, blurb_path, info.blurbs);

			return info;
		}
		catch (...)
		{
			std::cout << "Error reading json.\n";
		}
		return {};
	}

	void queue_game_image_fetches(time_point day_to_load, bool load_forward )
	{
		auto status = dates_being_loaded.get()->try_emplace( day_to_load, false );
		if (!status.second)
			return;
		//typical operation order is inverted due to chaining required by shared_call_on_death.
		auto game_infos = std::make_shared<mutexed_struct<required_json_info, std::shared_mutex>>();
		shared_call_on_death loading_flag([this, day_to_load]()
		{
			//same reason as why below
			//basically a lot of fun.. minor bugs (in the sense they don't ultimately matter)
			//can occur when you kill the program mid thread execution.
			//namely we can try to manipulate dead data because the program is closing and this thread hasn't finished yet.
			if (job_pool.terminate_threads)
				return;

			dates_being_loaded.get()->erase(day_to_load);
		});
		auto image_download_queuer = [game_infos, this, day_to_load, load_forward, loading_flag]()
		{
			auto protected_struct = game_infos->get_shared();
			if (protected_struct->images.empty())
			{
				time_point new_day_to_load;
				if (load_forward)
					new_day_to_load = next_day(day_to_load);
				else
					new_day_to_load = prior_day(day_to_load);

				//I don't know how to sanely stop loading in the future dates (e.g. today+1)
				//so just don't try to load them.
				if (day_to_load > std::chrono::system_clock::now())
					return;

				auto requeue_lam = [this, new_day_to_load, load_forward]()
				{
					queue_game_image_fetches(new_day_to_load, load_forward);
				};
				job_pool.add_job(requeue_lam);
				return;
			}
			auto iteration_lam = [=](auto zip_range)
			{
				std::size_t curr_ind = 0;
				std::size_t range_size = zip_range.end() - zip_range.begin();
				for (auto tile_create_info : zip_range)
				{
					//game_infos is captured so we don't have to copy the string to not have our references go out of scope.
					auto image_downloader_lam = [tile_create_info, game_infos, this, day_to_load, loading_flag, load_forward, curr_ind, range_size]()
					{
						//trying to determine if the mutex actually needs to be kept by this struct as we will never edit the data
						//nothing else will ever write to the structs again -- until this work is dead and done.
						//not to mention this will always be run in the same exact thread.....
						//because I'm paranoid I'll acquire it
						//we don't need to read anything though.
						auto game_info_locked = game_infos->get_shared();

						auto[image_url, headline, home_team, away_team, blurb] = tile_create_info;

						tile_creation_info tile_info;

						tile_info.tile_texels = std::move(read_from_url(image_url));
						tile_info.headline = std::move(headline);
						tile_info.home = std::move(home_team);
						tile_info.away = std::move(away_team);
						tile_info.blurb = std::move(blurb);
						tile_info.original_index = load_forward ? curr_ind : range_size - curr_ind;

						tile_info.query_date = day_to_load;
						tile_info.push_front = !load_forward;
						//done specifically because for whatever reason if the thread is meant to die and we push something onto our queue
						//tbb will throw an error. I'm not sure if this is on my end or theirs; but logically we don't need to update
						//the queue if we're never going to use the data..
						if (job_pool.terminate_threads)
							return;

						tiles_pending_upload.push(std::move(tile_info));
					};
					job_pool.add_job(std::move(image_downloader_lam));
					++curr_ind;
				}
			};
			if (load_forward)
				iteration_lam(protected_struct->get_zip_range());
			else
				iteration_lam(protected_struct->get_zip_range_reverse());
		};
		auto image_download_on_death_lam = [image_download_queuer, this]()
		{
			job_pool.add_job(image_download_queuer);
		};

		std::string day_to_load_str = date_to_iso_str(day_to_load);
		auto json_downloader_lam = [json_url = url_base + day_to_load_str + url_end, game_infos, image_download_call = shared_call_on_death(image_download_on_death_lam), this]()
		{
			rapidjson::Document document;
			document.Parse(read_from_url(json_url).str().c_str());
			*game_infos->get() = get_game_info(document);
		};

		job_pool.add_job(json_downloader_lam);
	}
};