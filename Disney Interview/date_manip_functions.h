#pragma once
#include <chrono>
#include <string>
#include <sstream>
#include <iomanip>
#include "string_manip_functions.h"

struct Tm : std::tm {
	Tm(const int year, const int month, const int mday, const int hour,
		const int min, const int sec, const int isDST = -1)
	{
		tm_year = year - 1900; // [0, 60] since 1900
		tm_mon = month - 1;    // [0, 11] since Jan
		tm_mday = mday;        // [1, 31]
		tm_hour = hour;        // [0, 23] since midnight
		tm_min = min;          // [0, 59] after the hour
		tm_sec = sec;          // [0, 60] after the min
							   //         allows for 1 positive leap second
		tm_isdst = isDST;      // [-1...] -1 for unknown, 0 for not DST,
							   //         any positive value if DST.
	}

	template <typename Clock_t = std::chrono::system_clock>
	auto to_time_point() -> typename Clock_t::time_point {
		auto time_c = mktime(this);
		return Clock_t::from_time_t(time_c);
	}
};



template<typename chrono_time_point_type>
std::string date_to_iso_str(chrono_time_point_type time_point)
{
	auto sys_clock = std::chrono::system_clock::to_time_t(time_point);
	std::stringstream stream;
	stream << std::put_time(std::localtime(&sys_clock), "%F");
	return stream.str();
}
auto str_to_chrono_time(const std::string& date_str)
{
	int year, month, day;
	int hour, min, sec;
	hour = min = sec = 0;
	auto t_pos = date_str.find('T');
	auto ymd_str = split_string(date_str.substr(0, t_pos), '-', 10000, ' ');

	year = std::stoi(ymd_str[0]);
	month = std::stoi(ymd_str[1]);
	day = std::stoi(ymd_str[2]);
	if (t_pos != std::string::npos)
	{
		auto hms_str = split_string(date_str.substr(t_pos + 1, -1), ':', 10000, ' ');

		hour = std::stoi(hms_str[0]);
		min = std::stoi(hms_str[1]);
		sec = std::stoi(hms_str[2]);

	}
	return Tm(year, month, day, hour, min, sec).to_time_point();
}
template<typename chrono_type>
chrono_type next_day(chrono_type time)
{
	return time + std::chrono::hours(24);
}
template<typename chrono_type>
chrono_type prior_day(chrono_type time)
{
	return time - std::chrono::hours(24);
}
