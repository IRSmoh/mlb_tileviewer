#pragma once
#include <string>
#include <vector>
#include <iostream>
//expects to be given a json object from rapidjson.
template<typename json_obj_type>
void json_reader(const json_obj_type& json_obj, const std::string& path, std::vector<std::string>& vec_to_fill, std::size_t curr_offset = 0)
{
	std::size_t slash_pos = path.find('/', curr_offset);
	auto json_query = path.substr(curr_offset, slash_pos - curr_offset);

	if (!json_obj.HasMember(json_query.c_str()))
	{
		std::cout << "Json parsing error looking for: " << json_query << "\n";
		return;
	}
	const auto& json_value = json_obj[json_query.c_str()];
	if (json_value.IsArray())
	{
		for (const auto& array_elem : json_value.GetArray())
		{
			json_reader(array_elem, path, vec_to_fill, slash_pos + 1);
		}
	}
	else if (json_value.IsObject())
	{
		json_reader(json_value, path, vec_to_fill, slash_pos + 1);
	}
	else if (json_value.IsString())
	{
		vec_to_fill.emplace_back(json_value.GetString());
	}
	else
	{
		std::cout << "Expected json nodes to be [array,object,string] failed to find any of the prior.\n";
	}
}