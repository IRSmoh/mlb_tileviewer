#pragma once
#include <vector>
#include <string>
std::vector<std::string> split_string(const std::string& str, char split_token, std::size_t or_split_at_pos, char space_token)
{
	std::vector<std::string> strings;
	std::size_t curr_pos = 0;
	while (true)
	{
		std::size_t split_pos = str.find(split_token, curr_pos);
		if ((split_pos - curr_pos) >= or_split_at_pos && curr_pos + or_split_at_pos < str.size())
		{
			split_pos = str.rfind(space_token, curr_pos + or_split_at_pos - 1);
			if (split_pos == std::string::npos)
				split_pos = str.rfind(space_token, curr_pos + or_split_at_pos - 1);
		}
		strings.emplace_back(str.substr(curr_pos, split_pos - curr_pos));
		if (split_pos == std::string::npos)
			break;
		curr_pos = split_pos + 1;
	}
	return strings;
}
