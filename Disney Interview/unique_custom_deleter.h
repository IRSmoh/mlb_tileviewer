#pragma once
#include <functional>
template<typename type>
struct unique_custom_deleter
{
	type data;
	std::function<void(type)> deleter;
	unique_custom_deleter() {}
	unique_custom_deleter(type data_, const std::function<void(type)>& deleter_) :
		data(data_), deleter(deleter_)
	{}
	unique_custom_deleter(const unique_custom_deleter&) = delete;
	unique_custom_deleter& operator=(const unique_custom_deleter&) = delete;
	unique_custom_deleter(unique_custom_deleter&& right)
	{
		cleanup();
		data = std::move(right.data);
		deleter = std::move(right.deleter);
	}

	unique_custom_deleter& operator=(unique_custom_deleter&& right)
	{
		cleanup();
		data = std::move(right.data);
		deleter = std::move(right.deleter);
		return *this;
	}
	~unique_custom_deleter()
	{
		cleanup();
	}
	operator type()
	{
		return data;
	}
	void cleanup()
	{
		if (deleter)
			deleter(data);
	}
};